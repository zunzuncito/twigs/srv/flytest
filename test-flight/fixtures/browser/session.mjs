

import Exec from 'zunzun/flyutil/exec.mjs'

export default class BrowserSession {
  static async construct(cfg, tools, subject_socket) {
    const _this = new BrowserSession(cfg, tools, subject_socket);
    return _this;
  }

  constructor(cfg, tools, subject_socket) {
    this.cfg = cfg;
    this.tools = tools;
    this.subject_socket = subject_socket;

    for (let srv of cfg.env.services) {
      if (srv.name == "authing") {
        this.test_cfg = srv.config.test;
        break;
      }
    }

  }

  async spawn() {
    this.process = Exec.spawn(`CHROMIUM_FLAGS="--disk-cache-dir=/dev/null --disk-cache-size=1" chromium --incognito --auto-open-devtools-for-tabs --new-window ${this.test_cfg.browser_entry_url}`);

    const _this = this;
    await new Promise((resolve) => {
      const waiterval = setInterval(() => {
        if (_this.ready) {
          clearInterval(waiterval);
          resolve();
        }
      }, 333);
    });
    console.log("BROWSER READY");
  }

  data(msg) {
    if (msg.notify) {
      if (msg.notify == "ready") {
        this.ready = true;
      } else if (msg.notify == "test-done") {
        this.test_done = true;
      }
    } else if (msg.path) {
      this.current_pathname = msg.path;
    }
  }

  async navigate(url) {
    this.ready = false;
    this.subject_socket.write(JSON.stringify({
      browser: {
        navigate: url
      }
    })+"\n");

    const _this = this;
    await new Promise((resolve) => {
      const waiterval = setInterval(() => {
        if (_this.ready) {
          clearInterval(waiterval);
          resolve();
        }
      }, 333);
    });
  }

  async reload() {
    this.ready = false;
    this.subject_socket.write(JSON.stringify({
      browser: {
        reload: true
      }
    })+"\n");

    const _this = this;
    await new Promise((resolve) => {
      const waiterval = setInterval(() => {
        if (_this.ready) {
          clearInterval(waiterval);
          resolve();
        }
      }, 333);
    });
  }

  async wait() {
    this.ready = false;

    const _this = this;
    await new Promise((resolve) => {
      const waiterval = setInterval(() => {
        if (_this.ready) {
          clearInterval(waiterval);
          resolve();
        }
      }, 333);
    });
  }

  async confirm_path(pathname) {
    const _this = this;
    await new Promise((resolve) => {
      const waiterval = setInterval(() => {
        if (_this.current_pathname == pathname) {
          clearInterval(waiterval);
          resolve();
        }
      }, 333);
    });
  }

  async test(target) {
    this.subject_socket.write(JSON.stringify({
      browser: {
        test: target
      }
    })+"\n");

    const _this = this;
    await new Promise((resolve) => {
      const waiterval = setInterval(() => {
        if (_this.test_done) {
          clearInterval(waiterval);
          _this.test_done = false;
          resolve();
        }
      }, 333);
    });
  }

  kill() {
    this.process.kill('SIGTERM');
  }

}
