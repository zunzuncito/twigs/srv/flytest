
import os from 'os'

import path from 'path'
import fs from 'fs'

import net from 'net'

import YAML from "yaml"
import { WebSocketServer } from 'ws';

import HookManager from 'zunzun/test-flight/hook/mg.mjs'

export default class FlyTest {
  constructor(sm, http, cfg) {
    this.http = http;
    this.cfg = cfg;

    this.project_twig_path = path.resolve(sm.path, "twig.json");
    try {
      this.project_twig_json = JSON.parse(fs.readFileSync(this.project_twig_path, 'utf8'));
    } catch (e) {
      console.error(e.stack);
    }

    const global_config_path = path.resolve(os.homedir(), '.zunzun/config.yaml');
    const global_config = YAML.parse(fs.readFileSync(global_config_path, 'utf8'));
    this.hook_mg = new HookManager(sm.path, cfg.hook_mg, this.project_twig_json, global_config, sm);

    const _this = this;

    const ws_server = new WebSocketServer({ server: this.http.server });
    let browser_socket = undefined;
    ws_server.on('connection', (socket) => {
      _this.hook_mg.set_browser_socket(socket);
      browser_socket = socket;
      console.log('  FlyTest > Established connection with web browser client!');

      socket.on('message', async (message) => {
        const msg = JSON.parse(message.toString());
        if (msg.task) {
          _this.hook_mg.tcp_client.write(JSON.stringify(msg)+"\n");
        } else {
          _this.hook_mg.tcp_client.write(JSON.stringify({
            browser: msg
          })+"\n");
        }
      });

      socket.on('close', () => {
          console.log('Client disconnected');
      });

      socket.on('error', (err) => {
          console.error('Socket error:', err);
      });

    });
  }

  static async start(sm, cfg) {
    try {
      const flyweb = sm.get_service_instance("flyweb");



      const _this = new FlyTest(sm, flyweb.http, cfg);



      return _this;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
}
